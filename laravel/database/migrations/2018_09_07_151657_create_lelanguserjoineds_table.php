<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLelanguserjoinedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lelanguserjoineds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->mediumText('file')->nullable();
            $table->integer('lelang_id')->unsigned();
            $table->foreign('lelang_id')
                ->references('id')->on('lelangs');
            $table->integer('user_umkm_id')->unsigned();
            $table->foreign('user_umkm_id')
                ->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lelanguserjoineds');
    }
}
