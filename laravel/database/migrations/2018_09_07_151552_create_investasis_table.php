<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investasis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->bigInteger('nominal');
            $table->mediumText('file_bukti_transfer')->nullable();
            $table->integer('inkubator_user_verifikator_id')->unsigned()->nullable();
            $table->foreign('inkubator_user_verifikator_id')
                ->references('id')->on('users');
            $table->integer('user_investor_id')->unsigned()->nullable();
            $table->foreign('user_investor_id')
                ->references('id')->on('users');
            $table->integer('user_umkm_id')->unsigned()->nullable();
            $table->foreign('user_umkm_id')
                ->references('id')->on('users');
            $table->integer('investasistatus_id')->unsigned()->nullable();
            $table->foreign('investasistatus_id')
                ->references('id')->on('investasistatuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investasis');
    }
}
