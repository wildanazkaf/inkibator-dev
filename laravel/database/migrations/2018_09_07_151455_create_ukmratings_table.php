<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUkmratingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ukmratings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->smallInteger('range');
            $table->text('description')->nullable();
            $table->mediumText('file')->nullable();
            $table->integer('ukm_id')->unsigned();
            $table->foreign('ukm_id')
                ->references('id')->on('ukms');
            $table->integer('coaching_id')->unsigned();
            $table->foreign('coaching_id')
                ->references('id')->on('coachings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ukmratings');
    }
}
