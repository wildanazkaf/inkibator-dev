<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachingmessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coachingmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->mediumText('attachment')->nullable();
            $table->integer('coachingmessagetype_id')->unsigned();
            $table->foreign('coachingmessagetype_id')
                ->references('id')->on('coachingmessagetypes');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->integer('coaching_id')->unsigned()->nullable();
            $table->foreign('coaching_id')
                ->references('id')->on('coachings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coachingmessages');
    }
}
