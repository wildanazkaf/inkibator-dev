<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->mediumText('address')->nullable();
            $table->mediumText('about')->nullable();
            $table->boolean('is_added_to_elearning')->nullable();
            $table->boolean('is_added_to_forum')->nullable();
            $table->integer('usertype_id')->unsigned();
            $table->foreign('usertype_id')
                ->references('id')->on('usertypes');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
