<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLelangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lelangs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->dateTime('waktu_mulai_lelang');
            $table->dateTime('waktu_selesai_lelang');
            $table->bigInteger('budget');
            $table->bigInteger('down_payment');
            $table->bigInteger('biaya_per_item')->nullable();
            $table->integer('jumlah_item');
            $table->integer('user_investor_id')->unsigned();
            $table->foreign('user_investor_id')
                ->references('id')->on('users');
            $table->integer('user_pemenang_umkm_id')->unsigned()->nullable();
            $table->foreign('user_pemenang_umkm_id')
                ->references('id')->on('users');
            $table->integer('lelangstatus_id')->unsigned();
            $table->foreign('lelangstatus_id')
                ->references('id')->on('lelangstatuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lelangs');
    }
}

