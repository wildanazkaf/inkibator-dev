<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUkmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ukms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('jenis_usaha');
            $table->integer('tahun_berdiri');
            $table->mediumText('alamat_usaha');
            $table->string('phone');
            $table->integer('jumlah_tenaga_kerja');
            $table->string('legalitas_usaha')->nullable();
            $table->string('penjualan_per_bulan');
            $table->integer('ukmcategory_id')->unsigned();
            $table->foreign('ukmcategory_id')
                ->references('id')->on('ukmcategories');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ukms');
    }
}
