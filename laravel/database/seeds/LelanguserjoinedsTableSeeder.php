<?php

use Illuminate\Database\Seeder;

class LelanguserjoinedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lelanguserjoineds')->insert([
            [
                'title' => 'Pengajuan Lelang Makanan Siang Geprek',
                'description' => 'Kami mengajukan penawaran menarik',
                'lelang_id' => 1,
                'user_umkm_id' => 2
            ],
            [
                'title' => 'Pengajuan Lelang Makanan Siang Geprek GeprukGeprek',
                'description' => 'Kami mengajukan penawaran menarik juga ',
                'lelang_id' => 1,
                'user_umkm_id' => 5
            ],
        ]);
    }
}
