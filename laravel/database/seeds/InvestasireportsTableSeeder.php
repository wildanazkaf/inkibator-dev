<?php

use Illuminate\Database\Seeder;

class InvestasireportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('investasireports')->insert([
            [
                'title' => 'Laporan Ivestasi Kerjasama Geprekers Bulan 1',
                'description' => 'Berikut ini saya lampirkan untuk laporan bekerjasama dengan UMKM Geprekers ',
                'investasi_id' => 1,
                'user_id' => 2,
            ]
        ]);
    }
}
