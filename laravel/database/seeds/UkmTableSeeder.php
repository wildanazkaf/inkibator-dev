<?php

use Illuminate\Database\Seeder;

class UkmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ukms')->insert([
            [
                'name' => 'Makanan',
                'jenis_usaha' => 'Makanan cepat saji',
                'tahun_berdiri' => 2007,
                'alamat_usaha' => 'Klampis ngasem, Surabaya',
                'phone' => '0856789098',
                'jumlah_tenaga_kerja' => 18,
                'legalitas_usaha' => 'HHK/090/JYIOL',
                'penjualan_per_bulan' => '100 item / hari',
                'ukmcategory_id' => 1,
                'user_id' => 2

            ]
        ]);
    }
}
