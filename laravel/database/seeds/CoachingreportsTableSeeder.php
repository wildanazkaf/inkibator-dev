<?php

use Illuminate\Database\Seeder;

class CoachingreportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coachingreports')->insert([
            [
                'title' => 'Laporan Kemajuan Minggu 4 Bulan Agustus 2018',
                'description' => 'Berikut ini adalah laporan kemajuannya',
                'user_id' => 2,
                'coaching_id' => 1,
                'coachingreporttype_id' => 1
            ]
        ]);
    }
}
