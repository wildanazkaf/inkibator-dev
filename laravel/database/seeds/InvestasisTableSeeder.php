<?php

use Illuminate\Database\Seeder;

class InvestasisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('investasis')->insert([
            [
                'title' => 'Ivestasi Kerjasama Geprekers',
                'description' => 'Saya tertarik untuk bekerjasama dengan UMKM Geprekers dan harapannya semakin menguntungkan dg kedua belah pihak',
                'nominal' => 30000000,
                'user_investor_id' => 4,
                'user_umkm_id' => 2,
                'investasistatus_id' => 1
            ]
        ]);
    }
}
