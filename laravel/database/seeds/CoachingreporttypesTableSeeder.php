<?php

use Illuminate\Database\Seeder;

class CoachingreporttypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coachingreporttypes')->insert([
            [
                'name' => 'Laporan Kemajuan'
            ],
            [
                'name' => 'Masalah',
            ],
            [
                'name' => 'Solusi',
            ]
        ]);
    }
}
