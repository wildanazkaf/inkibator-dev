<?php

use Illuminate\Database\Seeder;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notifications')->insert([
            [
                'title' => 'Tolong buka link berikut ini',
                'description' => ' dan selesaikan tugas e learning',
                'image' => 'https://i1.wp.com/notepam.com/wp-content/uploads/2017/10/resep-ayam-geprek-istimewa.jpg?resize=680%2C486',
                'is_read' => 0,
                'user_id' => 1,
                'user_tujuan_id' => 2
            ]
        ]);
    }
}
