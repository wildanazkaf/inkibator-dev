<?php

use Illuminate\Database\Seeder;

class UkmratingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ukmratings')->insert([
            [
                'title' => 'Progress Bagus!',
                'range' => 5,
                'description' => 'Kerja bagus!',
                'file' => 'https://i1.wp.com/notepam.com/wp-content/uploads/2017/10/resep-ayam-geprek-istimewa.jpg?resize=680%2C486',
                'ukm_id' => 1,
                'coaching_id' => 1,
            ],
            [
                'title' => 'Progress Bagus! 2',
                'range' => 4,
                'description' => 'Kerja cukup bagus!',
                'file' => 'https://i1.wp.com/notepam.com/wp-content/uploads/2017/10/resep-ayam-geprek-istimewa.jpg?resize=680%2C486',
                'ukm_id' => 1,
                'coaching_id' => 1,
            ]
        ]);
    }
}
