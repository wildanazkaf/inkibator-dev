<?php

use Illuminate\Database\Seeder;

class CoachingmessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coachingmessages')->insert([
            [
                'content' => 'Halo',
                'coachingmessagetype_id' => 1,
                'user_id' => 2,
                'coaching_id' => 1
            ],
            [
                'content' => 'Halo juga UKM Geprekers',
                'coachingmessagetype_id' => 1,
                'user_id' => 3,
                'coaching_id' => 1
            ],
            [
                'content' => 'Saya punya masalah untuk meningkatkan omset UKM Coach..',
                'coachingmessagetype_id' => 1,
                'user_id' => 2,
                'coaching_id' => 1
            ],
            [
                'content' => 'Tenang semua ada solusinya..',
                'coachingmessagetype_id' => 1,
                'user_id' => 3,
                'coaching_id' => 1
            ]
        ]);
    }
}
