<?php

use Illuminate\Database\Seeder;

class CoachingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coachings')->insert([
            [
                'ringkasan_masalah' => 'Omset belum banyak',
                'coachingstatus_id' => 2,
                'user_id' => 2
            ]
        ]);
    }
}
