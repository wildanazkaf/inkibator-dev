<?php

use Illuminate\Database\Seeder;

class CoachingstatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coachingstatuses')->insert([
            [
                'name' => 'Belum diassign Coach'
            ],
            [
                'name' => 'Sudah diassign coach',
            ],
            [
                'name' => 'Sesi konsultasi sedang berlangsung',
            ],
            [
                'name' => 'Sesi konsultasi selesai',
            ],
            [
                'name' => 'Sesi konsultasi diberhentikan',
            ]
        ]);
    }
}
