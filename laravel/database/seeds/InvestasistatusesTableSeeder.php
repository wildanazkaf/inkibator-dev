<?php

use Illuminate\Database\Seeder;

class InvestasistatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('investasistatuses')->insert([
            [
                'name' => 'Belum dikonfirmasi'
            ],
            [
                'name' => 'Sudah dikonfirmasi',
            ],
            [
                'name' => 'Menunggu Transfer',
            ],
            [
                'name' => 'Transfer Investor Diterima',
            ],
            [
                'name' => 'Investasi Berhasil Dilakukan',
            ],
            [
                'name' => 'Investasi Selesai',
            ]
        ]);
    }
}
