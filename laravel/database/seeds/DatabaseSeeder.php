<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsertypeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UkmcategoryTableSeeder::class);
        $this->call(UkmTableSeeder::class);
        $this->call(UkmproductsTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(CoachingstatusTableSeeder::class);
        $this->call(CoachingsTableSeeder::class);
        $this->call(UkmratingsTableSeeder::class);
        $this->call(CoachingmessagetypesTableSeeder::class);
        $this->call(CoachingmessagesTableSeeder::class);
        $this->call(CoachingreporttypesTableSeeder::class);
        $this->call(CoachingreportsTableSeeder::class);
        $this->call(InvestasistatusesTableSeeder::class);
        $this->call(InvestasisTableSeeder::class);
        $this->call(InvestasireportsTableSeeder::class);
        $this->call(LelangstatusesTableSeeder::class);
        $this->call(LelangsTableSeeder::class);
        $this->call(LelanguserjoinedsTableSeeder::class);

    }
}

