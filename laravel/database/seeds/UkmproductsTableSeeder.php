<?php

use Illuminate\Database\Seeder;

class UkmproductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ukmproducts')->insert([
            [
                'name' => 'Ayam Geprek',
                'description' => 'Makanan paling enak di Klampis',
                'image' => 'https://i1.wp.com/notepam.com/wp-content/uploads/2017/10/resep-ayam-geprek-istimewa.jpg?resize=680%2C486',
                'ukm_id' => 1
            ],
            [
                'name' => 'Ayam Geprek 2',
                'description' => 'Makanan paling enak di Klampis 2',
                'image' => 'https://i1.wp.com/notepam.com/wp-content/uploads/2017/10/resep-ayam-geprek-istimewa.jpg?resize=680%2C486',
                'ukm_id' => 1
            ]
        ]);
    }
}
