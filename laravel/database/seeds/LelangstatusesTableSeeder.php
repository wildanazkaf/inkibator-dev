<?php

use Illuminate\Database\Seeder;

class LelangstatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lelangstatuses')->insert([
            [
                'name' => 'Menunggu verifikasi'
            ],
            [
                'name' => 'Sudah diverivikasi'
            ],
            [
                'name' => 'Menunggu transfer pembayaran dari Investor'
            ],
            [
                'name' => 'Pembayaran dari Investor diterima'
            ],
            [
                'name' => 'Transfer Investor Diterima',
            ],
            [
                'name' => 'Proses lelang'
            ],
            [
                'name' => 'Berhasil mendapatkan pemenang lelang',
            ],
            [
                'name' => 'Menunggu komitmen fee dari pemenang lelang',
            ],
            [
                'name' => 'Lelang diproses oleh pemenang lelang',
            ],
            [
                'name' => 'Lelang selesai'
            ]
        ]);
    }
}
