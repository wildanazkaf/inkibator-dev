<?php

use Illuminate\Database\Seeder;

class UsertypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usertypes')->insert([
            [
                'name' => 'Administrator/Inkubator'
            ],
            [
                'name' => 'Umkm',
            ],
            [
                'name' => 'Coach',
            ],
            [
                'name' => 'Investor',
            ]
        ]);
    }
}
