<?php

use Illuminate\Database\Seeder;

class UkmcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ukmcategories')->insert([
            [
                'name' => 'Makanan'
            ],
            [
                'name' => 'Kerajinan',
            ],
            [
                'name' => 'Otomotif',
            ],
            [
                'name' => 'Fashion',
            ]
        ]);
    }
}
