<?php

use Illuminate\Database\Seeder;

class CoachingmessagetypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coachingmessagetypes')->insert([
            [
                'name' => 'text'
            ],
            [
                'name' => 'image',
            ],
            [
                'name' => 'video',
            ],
            [
                'name' => 'file',
            ],
            [
                'name' => 'link',
            ]
        ]);
    }
}
