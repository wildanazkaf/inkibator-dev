<?php

use Illuminate\Database\Seeder;

class LelangsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lelangs')->insert([
            [
                'title' => 'Lelang makan siang PT Investa Utama',
                'description' => 'Buat makan siang di kantor kami alamat Jl. Manyar 34 Sukolilo Surabaya, jam 13.00',
                'waktu_mulai_lelang' => \Carbon\Carbon::now(),
                'waktu_selesai_lelang' => \Carbon\Carbon::now()->addDays(7),
                'budget' => 3000000,
                'down_payment' => 1500000,
                'jumlah_item' => 250,
                'user_investor_id' => 4,
                'lelangstatus_id' => 1

            ]
        ]);
    }
}
