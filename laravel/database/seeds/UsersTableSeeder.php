<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Administrator User',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('secret'),
                'phone' => '031111111',
                'address' => 'Departemen Managemen Bisnis, ITS, Sukolilo, Surabaya',
                'about' => 'Departemen MB adalah salah satu departemen yang ada di ITS',
                'is_added_to_elearning' => 0,
                'is_added_to_forum' => 0,
                'usertype_id' => 1,
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Geprekers - UKM User',
                'email' => 'ukm@gmail.com',
                'password' => bcrypt('secret'),
                'phone' => '031111111',
                'address' => 'Klampis Ngasem, Sukolilo, Surabaya',
                'about' => 'Grprekers menjual Ayam Geprek cepat saji',
                'is_added_to_elearning' => 0,
                'is_added_to_forum' => 0,
                'usertype_id' => 2,
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Bang Ijal - Coach User',
                'email' => 'coach@gmail.com',
                'password' => bcrypt('secret'),
                'phone' => '032222222',
                'address' => 'Coach dari Manajemen Bisnis ITS, Sukolilo, Surabaya',
                'about' => 'Coach handal terpercaya',
                'is_added_to_elearning' => 0,
                'is_added_to_forum' => 0,
                'usertype_id' => 3,
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'PT Investa Utama - Investor',
                'email' => 'investor@gmail.com',
                'password' => bcrypt('secret'),
                'phone' => '0333333333',
                'address' => 'Investor dari PT Investa Utama, Surabaya',
                'about' => 'Investor masa depan',
                'is_added_to_elearning' => 0,
                'is_added_to_forum' => 0,
                'usertype_id' => 4,
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'GeprukGeprek - UKM User',
                'email' => 'ukm1@gmail.com',
                'password' => bcrypt('secret'),
                'phone' => '034444444',
                'address' => 'Keputih, Sukolilo, Surabaya',
                'about' => 'GeprukGeprek menjual Ayam Geprek cepat saji',
                'is_added_to_elearning' => 0,
                'is_added_to_forum' => 0,
                'usertype_id' => 2,
                'created_at' => Carbon::now(),
            ],
        ]);
    }
}
