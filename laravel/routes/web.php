<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('page');;
//Admin
Route::resource('user','UserController');
Route::resource('coaching','CoachController');
Route::resource('investasi','InvestasiController');
Route::resource('umkm','UmkmController');
Route::resource('lelang','LelangController');

Auth::routes();
Route::group(['prefix' => 'admin'], function(){
    Route::get('/home', 'HomeController@index');
    Route::get('/users', 'UserController@index');
    Route::get('/lelang', 'LelangController@index');
    Route::get('/umkm', 'UmkmController@index');
    Route::get('/investasi', 'InvestasiController@index');
    Route::get('/coaching', 'CoachController@index')->name('coaching');
    Route::get('/coaching/assign/{id}', 'CoachController@assign')->name('coaching.assign');
    Route::put('/coaching/assigned/{id}', 'CoachController@assigned')->name('coaching.assigned');
});

Route::group(['prefix' => 'umkmuser'], function(){
    Route::get('/home', 'Umkmuser\UmkmController@index');
    Route::get('/profil', 'Umkmuser\UmkmController@profil');
    Route::get('/edit', 'Umkmuser\UmkmController@edit');
    Route::get('/chat', 'Umkmuser\UmkmController@chat');
    Route::get('/product', 'Umkmuser\UmkmController@product');
});
Route::resource('umkmuser','Umkmuser\UmkmController');


Route::group(['prefix' => 'investor'], function(){
    Route::get('/home', 'Investor\InvestorController@index');
    Route::get('/profil', 'Investor\InvestorController@profil');
    Route::get('/investasi', 'Investor\InvestorController@investasi');
    Route::get('/edit', 'Investor\InvestorController@edit');
    Route::get('/listumkm', 'Investor\InvestorController@listumkm');
    Route::get('/umkmdetail/{id}', 'Investor\InvestorController@umkmdetail')->name('investor.umkmdetail');
});
Route::resource('investor','Investor\InvestorController');


Route::group(['prefix' => 'coachuser'], function(){
    Route::get('/home', 'Coachuser\CoachController@index');
    Route::get('/profil', 'Coachuser\CoachController@profil');
    Route::get('/chat', 'Coachuser\CoachController@chat');
    Route::get('/edit', 'Coachuser\CoachController@edit');
});

Route::resource('coachuser','Coachuser\CoachController');
