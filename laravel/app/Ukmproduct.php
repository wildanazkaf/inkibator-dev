<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ukmproduct extends Model
{
    protected $fillable = [
        'name', 'description', 'image'
    ];

    public function ukm()
    {
        return $this->belongsTo(Ukm::class);
    }
}
