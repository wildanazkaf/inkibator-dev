<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function usertype() {
        return $this->belongsTo(Usertype::class);
    }

    public function ukm()
    {
        return $this->hasOne(Ukm::class,'user_id');
    }

    public function notification()
    {
        return $this->hasMany(Notification::class);
    }

    public function notificationtujuan()
    {
        return $this->hasMany(Notification::class);
    }

    public function investasiinvestors()
    {
        return $this->hasMany(Investasi::class, 'user_investor_id');
    }

    public function investasiukms()
    {
        return $this->hasMany(Investasi::class, 'user_umkm_id');
    }

    public function coachingukms()
    {
        return $this->hasMany(Coaching::class, 'user_id');
    }

    public function coachingcoachs()
    {
        return $this->hasMany(Coaching::class, 'user_coach_id');
    }

    public function investasireports()
    {
        return $this->hasMany(Investasireport::class);
    }

    public function userjoinelelangs()
    {
        return $this->hasMany(Lelanguserjoined::class, 'user_umkm_id');
    }

    public function authorizeRoles($role)
    {
        return $this->hasRole($role) ||
            abort(401, 'This action is unauthorized.');

    }
    public function hasRole($role)
    {
        return null !== $this->usertype()->where('id', $role)->first();
    }
}
