<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coachingreport extends Model
{
    protected $fillable = [
        'title', 'description', 'file', 'user_id', 'coaching_id', 'coachingreporttype_id'
    ];

    public function coachingreporttype()
    {
        return $this->belongsTo(Coachingreporttype::class);
    }

    public function coaching()
    {
        return $this->belongsTo(Coaching::class);
    }
}
