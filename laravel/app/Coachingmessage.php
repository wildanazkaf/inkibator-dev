<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coachingmessage extends Model
{
    protected $fillable = [
        'content', 'attachment', 'coachingmessagetype_id', 'user_id', 'coaching_id'
    ];

    public function coachingmessagetype()
    {
        return $this->belongsTo(Coachingmessagetype::class);
    }

    public function coaching()
    {
        return $this->belongsTo(Coaching::class);
    }
}
