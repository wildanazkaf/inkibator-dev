<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ukmcategory extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    public function ukms() {
        return $this->hasMany(Ukm::class);
    }
}
