<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coachingstatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public function coachings()
    {
        return $this->hasMany(Coaching::class);
    }
}
