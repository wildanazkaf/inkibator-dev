<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investasi extends Model
{
    protected $fillable = [
        'title', 'description', 'nominal', 'file_bukti_transfer', 'user_investor_id', 'user_umkm_id', 'inkubator_user_verifikator_id',
        'investasistatus_id'
    ];

    public function investasistatus()
    {
        return $this->belongsTo(Investasistatus::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_investor_id');
    }

    public function umkm()
    {
        return $this->belongsTo(User::class,'user_umkm_id');
    }

    public function inkubator()
    {
        return $this->belongsTo(User::class,'inkubator_user_verifikator_id');
    }

    public function investasireports()
    {
       return  $this->hasMany(Investasireport::class);
    }
}
