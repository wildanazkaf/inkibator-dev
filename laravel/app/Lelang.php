<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lelang extends Model
{
    protected $fillable = [
        'title', 'description', 'waktu_mulai_lelang', 'waktu_selesai_lelang', 'budget', 'down_payment', 'biaya_per_item', 'jumlah_item',
        'user_investor_id', 'user_pemenang_umkm_id', 'lelangstatus_id'

    ];

    public function lelangstatus()
    {
        return $this->belongsTo(Lelangstatus::class);
    }

    public function userinvestorlelangowner()
    {
        return $this->belongsTo(User::class, 'user_investor_id');
    }

    public function userumkmpemenanglelang()
    {
        return $this->belongsTo(User::class, 'user_pemenang_umkm_id');
    }

    public function userumkmjoinlelang()
    {
        return $this->hasMany(Lelanguserjoined::class);
    }
}
