<?php

namespace App\Http\Controllers;

use App\Ukm;
use App\Ukmcategory;
use Illuminate\Http\Request;

class UmkmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['1']);

        $umkmkat = Ukmcategory::all();
        return view('admin.umkm.umkm', compact('umkmkat'));
    }
    public function create(Request $request)
    {
        return view('admin.umkm.register');
    }
    public function store(Request $request)
    {
        $data = new Ukmcategory();
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save();
        return redirect()->route('umkm.index')->with('alert-success','Berhasil Menambahkan Data!');
    }
    public function edit($id)
    {
        $data = Ukmcategory::where('id',$id)->get();

        return view('admin.umkm.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = Ukmcategory::where('id',$id)->first();
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save();
        return redirect()->route('umkm.index')->with('alert-success','Data berhasil diubah!');
    }
    public function destroy($id)
    {
        $data = Ukmcategory::where('id',$id)->first();
        $data->delete();
        return redirect()->route('umkm.index')->with('alert-success','Data berhasi dihapus!');
    }
}
