<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coaching;
use App\User;

class CoachController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        //cho Auth::user()->usertype->name;
        //echo Auth::user();
        //echo User::all();
        $coaching = Coaching::all();
        return view('admin.coaching.coachs', compact('coaching'));
    }
    public function edit($id)
    {
        $data = Coaching::where('id',$id)->get();

        return view('admin.coaching.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = Coaching::where('id',$id)->first();
        $data->ringkasan_masalah = $request->masalah;
        $data->coachingstatus_id = $request->status;
        $data->user_id= $request->user;
        $data->save();
        return redirect()->route('coaching')->with('alert-success','Data berhasil diubah!');
    }
    public function destroy($id)
    {
        $data = Coaching::where('id',$id)->first();
        $data->delete();
        return redirect()->route('coaching')->with('alert-success','Data berhasi dihapus!');
    }
    public function assign()
    {
        return view('admin.coaching.assign', ['data' => User::where('usertype_id',3)->get()], ['coacher' => Coaching::all()]);
    }
    public function assigned(Request $request, $id)
    {
        $data = Coaching::where('id',$id)->first();
        $data->user_coach_id = $request->coach;
        $data->save();
        return redirect()->route('coaching')->with('alert-success','Data berhasil diubah!');
    }
}