<?php

namespace App\Http\Controllers\Investor;


use App\Http\Controllers\Controller;
use App\Ukm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvestorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('investor.home');
    }
    public function profil()
    {
        $data = Auth::user();

        return view('investor.profil',compact('data'));
    }
    public function edit()
    {
        $id = Auth::user()->id;
        $data = User::where('id',$id)->first();

        return view('investor.edit',compact('data'));
    }
    public function update(Request $request)
    {
        $id = Auth::user()->id;
        $data = User::where('id',$id)->first();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->address = $request->address;
        $data->about = $request->about;
        $data->save();
        return redirect()->route('investor.index')->with('alert-success','Data berhasil diubah!');
    }
    public function investasi()
    {
        $ukm = Ukm::all();
        return view('investor.investasi', compact('ukm'));
    }
    public function listumkm()
    {
        $users = User::where('usertype_id', 2)->get();
        return view('investor.listumkm', compact('users'));
    }
    public function umkmdetail($id)
    {
        $users = User::find($id);
        return view('investor.umkmdetail', compact('users','id'));
    }

}