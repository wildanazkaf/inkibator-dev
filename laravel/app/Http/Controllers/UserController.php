<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['1']);
        //cho Auth::user()->usertype->name;
        //echo Auth::user();
        //echo User::all();
        $users = User::all();
        return view('admin.users', compact('users'));
    }
    public function create(Request $request)
    {
        return view('admin.register');
    }
    public function store(Request $request)
    {
        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->phone = $request->phone;
        $data->address = $request->address;
        $data->about = $request->about;
        $data->usertype_id = $request->usertype;
        $data->is_added_to_elearning = $request->is_added_to_elearning;
        $data->is_added_to_forum = $request->is_added_to_forum;
        $data->save();
        return redirect()->route('user.index')->with('alert-success','Berhasil Menambahkan Data!');
    }
    public function edit($id)
    {
        $data = User::where('id',$id)->get();

        return view('admin.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = User::where('id',$id)->first();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->phone = $request->phone;
        $data->address = $request->address;
        $data->about = $request->about;
        $data->is_added_to_elearning = $request->is_added_to_elearning;
        $data->is_added_to_forum = $request->is_added_to_forum;
        $data->save();
        return redirect()->route('user.index')->with('alert-success','Data berhasil diubah!');
    }
    public function destroy($id)
    {
        $data = User::where('id',$id)->first();
        $data->delete();
        return redirect()->route('user.index')->with('alert-success','Data berhasi dihapus!');
    }
}
