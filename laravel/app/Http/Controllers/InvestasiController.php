<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investasi;

class InvestasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $investasi = Investasi::all();
        return view('admin.investasi.investasi', compact('investasi'));
    }
    public function edit($id)
    {
        $data = Investasi::where('id',$id)->get();

        return view('admin.investasi.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = Investasi::where('id',$id)->first();
        $data->title = $request->title;
        $data->description = $request->description;
        $data->nominal = $request->nominal;
        $data->inkubator_user_verifikator_id = $request->inkubator;
        $data->investasistatus_id = $request->status;
        $data->save();
        return redirect()->route('investasi.index')->with('alert-success','Data berhasil diubah!');
    }
    public function destroy($id)
    {
        $data = Investasi::where('id',$id)->first();
        $data->delete();
        return redirect()->route('investasi.index')->with('alert-success','Data berhasi dihapus!');
    }
}