<?php

namespace App\Http\Controllers;

use App\Lelang;
use App\Ukm;
use App\Ukmcategory;
use Illuminate\Http\Request;

class LelangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['1']);

        $lelangs = Lelang::all();
        return view('admin.lelang.lelang', compact('lelangs'));
    }
    public function create(Request $request)
    {
        return view('admin.umkm.register');
    }
    public function store(Request $request)
    {
        $data = new Ukmcategory();
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save();
        return redirect()->route('lelang.index')->with('alert-success','Berhasil Menambahkan Data!');
    }
    public function edit($id)
    {
        $data = Lelang::where('id',$id)->get();

        return view('admin.lelang.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = Lelang::where('id',$id)->first();
        $data->title = $request->title;
        $data->description = $request->description;
        $data->waktu_mulai_lelang = $request->waktu_mulai_lelang;
        $data->waktu_selesai_lelang = $request->waktu_selesai_lelang;
        $data->budget = $request->budget;
        $data->down_payment = $request->down_payment;
        $data->biaya_per_item = $request->biaya_per_item;
        $data->jumlah_item = $request->jumlah_item;
        $data->user_investor_id = $request->user_investor_id;
        $data->user_pemenang_umkm_id = $request->user_pemenang_umkm_id;
        $data->lelangstatus_id = $request->lelangstatus_id;
        $data->save();
        return redirect()->route('lelang.index')->with('alert-success','Data berhasil diubah!');
    }
    public function destroy($id)
    {
        $data = Lelang::where('id',$id)->first();
        $data->delete();
        return redirect()->route('lelang.index')->with('alert-success','Data berhasi dihapus!');
    }
}
