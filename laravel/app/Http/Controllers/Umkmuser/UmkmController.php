<?php

namespace App\Http\Controllers\Umkmuser;

use App\Http\Controllers\Controller;
use App\Ukm;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class UmkmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //$request->user()->authorizeRoles(['1']);
        //cho Auth::user()->usertype->name;

        //echo User::all();
        //$userid = Auth::user()->id;
        //$users = Auth::user();
        return view('umkm.home');
    }
    public function profil()
    {
        $data = Auth::user();

        return view('umkm.profil',compact('data'));
    }
    public function edit()
    {
        $id = Auth::user()->id;
        $datas = User::where('id',$id)->first();

        return view('umkm.edit',compact('datas'));
    }
    public function chat()
    {
        return view('umkm.chat');
    }
    public function product()
    {
        return view('umkm.product');
    }
    public function update(Request $request)
    {
        $id = Auth::user()->id;
        $data = User::where('id',$id)->first();
        $data->ukm->name = $request->name;
        $data->ukm->jenis_usaha = $request->jenis_usaha;
        $data->ukm->tahun_berdiri = $request->tahun_berdiri;
        $data->phone = $request->phone;
        $data->ukm->jumlah_tenaga_kerja = $request->jumlah_tenaga_kerja;
        $data->ukm->legalitas_usaha = $request->legalitas_usaha;
        $data->ukm->penjualan_per_bulan = $request->penjualan_per_bulan;
        $data->email = $request->email;
        $data->address = $request->address;
        $data->ukm->save();
        $data->save();
        return redirect()->route('umkmuser.index')->with('alert-success','Data berhasil diubah!');
    }
    public function destroy($id)
    {
        $data = User::where('id',$id)->first();
        $data->delete();
        return redirect()->route('umkm.index')->with('alert-success','Data berhasi dihapus!');
    }
}
