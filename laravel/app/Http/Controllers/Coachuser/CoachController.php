<?php

namespace App\Http\Controllers\Coachuser;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoachController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('coach.home');
    }
    public function profil()
    {
        $data = Auth::user();

        return view('coach.profil',compact('data'));
    }
    public function chat()
    {
        return view('coach.chat');
    }
    public function edit()
    {
        $id = Auth::user()->id;
        $data = User::where('id',$id)->first();

        return view('coach.edit',compact('data'));
    }
    public function update(Request $request)
    {
        $id = Auth::user()->id;
        $data = User::where('id',$id)->first();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->address = $request->address;
        $data->about = $request->about;
        $data->save();
        return redirect()->route('coachuser.index')->with('alert-success','Data berhasil diubah!');
    }

}