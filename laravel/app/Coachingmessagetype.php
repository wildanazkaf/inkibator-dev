<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coachingmessagetype extends Model
{
    protected $fillable = [
        'name'
    ];

    public function coachingmessages()
    {
        return $this->hasMany(Coachingmessage::class);
    }
}
