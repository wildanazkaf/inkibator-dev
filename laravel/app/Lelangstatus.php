<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lelangstatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public function lelangs()
    {
        return $this->hasMany(Lelang::class);
    }
}
