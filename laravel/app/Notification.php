<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'title', 'description', 'image', 'is_read',
        'user_id', 'user_tujuan_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function usertujuan()
    {
        $this->belongsTo(User::class);
    }
}
