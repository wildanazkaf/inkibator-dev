<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coaching extends Model
{
    protected $fillable = [
        'ringkasan_masalah', 'coachingstatus_id', 'user_id', 'user_coach_id'
    ];

    public function coachingstatus()
    {
        return $this->belongsTo(Coachingstatus::class);
    }

    public function user() {
        return $this->belongsTo(User::class,'user_id');
    }

    public function userCoach() {
        return $this->belongsTo(User::class, 'user_coach_id');
    }

    public function coachingmessages()
    {
        return $this->hasMany(Coachingmessage::class);
    }

    public function coachingreports()
    {
        return $this->hasMany(Coachingreport::class);
    }
}
