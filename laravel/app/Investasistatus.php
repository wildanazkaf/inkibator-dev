<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investasistatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public function investasis()
    {
        return $this->hasMany(Investasi::class);
    }
}
