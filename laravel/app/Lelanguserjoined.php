<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lelanguserjoined extends Model
{
    protected $fillable = [
        'title', 'description', 'file',
        'lelang_id', 'user_umkm_id'
    ];

    public function lelang()
    {
        return $this->belongsTo(Lelang::class);
    }

    public function userumkm()
    {
        return $this->belongsTo(User::class);
    }
}
