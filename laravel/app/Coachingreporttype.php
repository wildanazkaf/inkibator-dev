<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coachingreporttype extends Model
{
    protected $fillable = [
        'name'
    ];

    public function coachingreports()
    {
        return $this->hasMany(Coachingreport::class);
    }

}
