<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ukmrating extends Model
{
    protected $fillable = [
        'title', 'range', 'description', 'file', 'umkm_id',  'coaching_id'
    ];

    public function ukm()
    {
        $this->belongsTo(Ukm::class);
    }
}
