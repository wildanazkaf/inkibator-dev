<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ukm extends Model
{
    protected $fillable = [
        'name', 'jenis_usaha', 'jenis_usaha', 'tahun_berdiri',
        'alamat_usaha', 'phone', 'jumlah_tenaga_kerja', 'legalitas_usaha',
        'penjualan_per_bulan', 'ukmcategory_id'
    ];

    public function ukmcategory()
    {
        return $this->belongsTo(Ukmcategory::class);
    }

    public function products()
    {
        return $this->hasMany(Ukmproduct::class);
    }

    public function ratings()
    {
        return $this->hasMany(Ukmrating::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
