<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investasireport extends Model
{
    protected $fillable = [
        'title', 'description', 'file', 'investasi_id'
    ];

    public function investasi()
    {
        return $this->belongsTo(Investasi::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
