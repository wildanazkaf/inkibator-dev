<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => "Ichiro ITS Robotics Team", // set false to total remove
            'description'  => 'Ichiro ITS Robotics Team - We develop humanoid and soccer robot', // set false to total remove
            'separator'    => ' - ',
            'keywords'     => ['ichiro its', 'ichiro its robotics team', 'tim robot its', 'robot its', 'robot institut teknologi sepuluh nopember surabaya', 'robot sepak bola indonesia', 'robot humanoid indonesia', 'robot sepak bola'],
            'canonical'    => null, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Ichiro ITS', // set false to total remove
            'description' => 'Ichiro ITS Robotics Team', // set false to total remove
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => false,
            'site_name'   => false,
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ],
];
