<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{url('investor/umkm')}}">
                    <i class="fa fa-table"></i> <span>List Umkm</span>
                </a>
            </li>
            <li>
                <a href="{{url('investor/investasi')}}">
                    <i class="fa fa-table"></i> <span>Investasi</span>
                </a>
            </li>
            <li>
                <a href="{{url('investor/investasi')}}">
                    <i class="fa fa-table"></i> <span>Riwayat Investasi</span>
                </a>
            </li>
            <li>
                <a href="{{url('investor/profil')}}">
                    <i class="fa fa-table"></i> <span>Profile</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>