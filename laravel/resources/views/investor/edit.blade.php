@extends('investor.investor')

@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">

        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Edit Profil Investor</h1>
                <hr>
                    <form action="{{ route('investor.update', $data) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="name">Nama Investor:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email Investor:</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{ $data->email }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone:</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $data->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat:</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{ $data->address }}">
                        </div>
                        <div class="form-group">
                            <label for="about">About:</label>
                            <input type="text" class="form-control" id="about" name="about" value="{{ $data->about }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger" onclick="history.back();">Cancel</button>
                        </div>
                    </form>
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection