@extends('investor.investor')
@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Investasi Baru</h1>
                <hr>
                    <form action="" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Nama UKM:</label>
                            <select class="form-control m-bot15" name="list_ukm">
                                @if($ukm->count() > 0)
                                    @foreach($ukm as $i)
                                        <option value="{{$i->id}}">{{$i->user->name}}</option>
                                    @endForeach
                                @else
                                    No Record Found
                                @endif
                            </select></div>
                        <div class="form-group">
                            <label for="description">Nama Investasi:</label>
                            <input type="text" class="form-control" id="description" name="description" value="">
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi:</label>
                            <input type="text" class="form-control" id="description" name="description" value="">
                        </div>
                        <div class="form-group">
                            <label for="description">Nominal:</label>
                            <input type="text" class="form-control" id="description" name="description" value="">
                        </div>
                        <div class="form-group">
                            <label for="description">File Bukti Transfer:</label>
                            <input type="file" name="fileToUpload" id="fileToUpload">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                        </div>
                    </form>
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection