@extends('umkm.umkm')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Profil UMKM</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Jenis UKM</th>
                                    <td>{{ $data->ukm->name }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Usaha</th>
                                    <td>{{ $data->ukm->jenis_usaha }}</td>
                                </tr>
                                <tr>
                                    <th>Tahun Berdiri</th>
                                    <td>{{ $data->ukm->tahun_berdiri }}</td>

                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td>{{ $data->phone }}</td>
                                </tr>
                                <tr>
                                    <th>Jumlah Tenaga Kerja</th>
                                    <td>{{ $data->ukm->jumlah_tenaga_kerja }}</td>
                                </tr>
                                <tr>
                                    <th>Legalitas Usaha</th>
                                    <td>{{ $data->ukm->legalitas_usaha }}</td>
                                </tr>
                                <tr>
                                    <th>Penjualan Per Bulan</th>
                                    <td>{{ $data->ukm->penjualan_per_bulan }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $data->email }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td>{{ $data->address }}</td>
                                </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <form action="{{ route('umkmuser.destroy', $data) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('umkmuser.edit',$data) }}" class=" btn btn-sm btn-primary">Edit</a>
                                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
