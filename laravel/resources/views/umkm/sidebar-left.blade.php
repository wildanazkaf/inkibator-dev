<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{url('umkmuser/chat')}}">
                    <i class="fa fa-table"></i> <span>Chat</span>
                </a>
            </li>

            <li>
                <a href="">
                    <i class="fa fa-table"></i> <span>Riwayat Coaching</span>
                </a>
            </li>
            <li>
                <a href="{{url('umkmuser/product')}}">
                    <i class="fa fa-table"></i> <span>List Produk</span>
                </a>
            </li>
            <li>
                <a href="{{url('umkmuser/profil')}}">
                    <i class="fa fa-table"></i> <span>Profile</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>