@extends('umkm.umkm')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Profil UMKM</h3>
                            <a href="" class=" btn btn-sm btn-primary">Edit</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{ $users->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>User Type</td>
                                        <td>{{ $users->usertype->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis UMKM</td>
                                        <td>{{ $users->ukm->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Usaha</td>
                                        <td>{{ $users->ukm->jenis_usaha }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Berdiri</td>
                                        <td>{{ $users->ukm->tahun_berdiri }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat Usaha</td>
                                        <td>{{ $users->ukm->alamat_usaha }}</td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>{{ $users->ukm->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Tenaga Kerja</td>
                                        <td>{{ $users->ukm->jumlah_tenaga_kerja }}</td>
                                    </tr>
                                    <tr>
                                        <td>Legaltias Usaha</td>
                                        <td>{{ $users->ukm->legalitas_usaha }}</td>
                                    </tr>
                                    <tr>
                                        <td>Penjualan Per Bulan</td>
                                        <td>{{ $users->ukm->penjualan_per_bulan }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
