@extends('umkm.umkm')

@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">

        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Edit Profil UMKM</h1>
                <hr>
                    <form action="{{ route('umkmuser.update', $datas) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="name">Jenis UKM:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $datas->ukm->name }}">
                        </div>
                        <div class="form-group">
                            <label for="jenis_usaha">Jenis Usaha:</label>
                            <input type="text" class="form-control" id="jenis_usaha" name="jenis_usaha" value="{{ $datas->ukm->jenis_usaha }}">
                        </div>
                        <div class="form-group">
                            <label for="tahun_berdiri">Tahun Berdiri:</label>
                            <input type="text" class="form-control" id="tahun_berdiri" name="tahun_berdiri" value="{{ $datas->ukm->tahun_berdiri }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone:</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $datas->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="jumlah_tenaga_kerja">Jumlah Tenaga Kerja:</label>
                            <input type="text" class="form-control" id="jumlah_tenaga_kerja" name="jumlah_tenaga_kerja" value="{{ $datas->ukm->jumlah_tenaga_kerja }}">
                        </div>
                        <div class="form-group">
                            <label for="legalitas_usaha">Legalitas Usaha:</label>
                            <input type="text" class="form-control" id="legalitas_usaha" name="legalitas_usaha" value="{{ $datas->ukm->legalitas_usaha }}">
                        </div>
                        <div class="form-group">
                            <label for="penjualan_per_bulan">Penjualan Per Bulan:</label>
                            <input type="text" class="form-control" id="penjualan_per_bulan" name="penjualan_per_bulan" value="{{ $datas->ukm->penjualan_per_bulan }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{ $datas->email }}">
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat:</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{ $datas->address }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger" onclick="history.back();">Cancel</button>
                        </div>
                    </form>
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection