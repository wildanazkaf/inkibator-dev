@extends('admin.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Tambah User</h1>
                <hr>
                <form action="{{ route('user.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="nama">Nama:</label>
                        <input type="text" class="form-control" id="user" name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="text" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="phone">Telepon:</label>
                        <input type="text" class="form-control" id="phone" name="phone">
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat:</label>
                        <input type="textarea" class="form-control" id="address" name="address">
                    </div>
                    <div class="form-group">
                        <label for="about">Keterangan :</label>
                        <input type="text" class="form-control" id="about" name="about">
                    </div>
                    <div class="form-group">
                        <label for="usertype">Usertype:</label>
                        <select class="form-control" id="usertype" name="usertype">
                            <option value="1">Administrator</option>
                            <option value="2">Umkm</option>
                            <option value="3">Coach</option>
                            <option value="4">Investor</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="is_added_to_elearning">Apakah sudah masuk E-learning:</label><br />
                        <input type="radio" id="is_added_to_elearning" name="is_added_to_elearning" value="1">Sudah
                        <input type="radio" id="is_added_to_elearning" name="is_added_to_elearning" value="0">Belum
                    </div>
                    <div class="form-group">
                        <label for="is_added_to_forum">Apakah sudah masuk Forum:</label><br />
                        <input type="radio" id="is_added_to_forum" name="is_added_to_forum" value="1">Sudah
                        <input type="radio" id="is_added_to_forum" name="is_added_to_forum" value="0">Belum
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Submit</button>
                        <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
