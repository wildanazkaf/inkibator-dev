@extends('admin.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Lelang Management</h3>
                            <a href="" class=" btn btn-sm btn-primary">Create</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Waktu Mulai</th>
                                    <th>Waktu Selesai</th>
                                    <th>Budget</th>
                                    <th>Down Payment</th>
                                    <th>Biaya/Item</th>
                                    <th>Jumlah Item</th>
                                    <th>Pelelang</th>
                                    <th>Pemenang</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($lelangs as $i => $lelang)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $lelang->title }}</td>
                                        <td>{{ $lelang->description }}</td>
                                        <td>{{ $lelang->waktu_mulai_lelang }}</td>
                                        <td>{{ $lelang->waktu_selesai_lelang }}</td>
                                        <td>{{ $lelang->budget }}</td>
                                        <td>{{ $lelang->down_payment }}</td>
                                        <td>{{ $lelang->biaya_per_item }}</td>
                                        <td>{{ $lelang->jumlah_item }}</td>
                                        <td>{{ $lelang->userinvestorlelangowner->name }}</td>
                                        @if(is_null($lelang->user_pemenang_umkm_id))
                                            <td>Belum ada Pemenang</td>
                                        @else()
                                            <td>{{ $lelang->userumkmpemenanglelang->name }}</td>
                                        @endif
                                        <td>{{ $lelang->lelangstatus->name }}</td>

                                        <td>
                                            <form action="{{ route('lelang.destroy', $lelang->id) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('lelang.edit',$lelang->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
