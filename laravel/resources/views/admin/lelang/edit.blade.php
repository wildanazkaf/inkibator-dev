@extends('admin.admin')
@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">

        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Edit Lelang</h1>
                <hr>
                @foreach($data as $datas)
                    <form action="{{ route('lelang.update', $datas->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $datas->title }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description:</label>
                            <input type="text" class="form-control" id="description" name="description" value="{{ $datas->description }}">
                        </div>
                        <div class="form-group">
                            <label for="waktu_mulai_lelang">Waktu Mulai Lelang:</label>
                            <input type="text" class="timepicker form-control" id="waktu_mulai_lelang" name="waktu_mulai_lelang" value="{{ $datas->waktu_mulai_lelang }}">
                        </div>
                        <div class="form-group">
                            <label for="waktu_selesai_lelang">Waktu Selesai Lelang:</label>
                            <input type="text" class="timepicker form-control" id="waktu_selesai_lelang" name="waktu_selesai_lelang" value="{{ $datas->waktu_selesai_lelang }}">
                        </div>
                        <div class="form-group">
                            <label for="budget">Budget:</label>
                            <input type="text" class="form-control" id="budget" name="budget" value="{{ $datas->budget }}">
                        </div>
                        <div class="form-group">
                            <label for="down_payment">Down Payment:</label>
                            <input type="text" class="form-control" id="down_payment" name="down_payment" value="{{ $datas->down_payment }}">
                        </div>
                        <div class="form-group">
                            <label for="biaya_per_item">Biaya Per Item:</label>
                            <input type="text" class="form-control" id="biaya_per_item" name="biaya_per_item" value="{{ $datas->biaya_per_item }}">
                        </div>
                        <div class="form-group">
                            <label for="jumlah_item">Jumlah item:</label>
                            <input type="text" class="form-control" id="jumlah_item" name="jumlah_item" value="{{ $datas->jumlah_item }}">
                        </div>
                        <div class="form-group">
                            <label for="user_investor_id">Pelelang:</label>
                            <select name="user_investor_id">
                                @foreach($datas->userinvestorlelangowner->where('usertype_id','=','4')->get() as $pelelang)
                                    <option class="form-control" id="inkubator" value="{{ $pelelang->id }}" {{ $datas->user_investor_id == $pelelang->id ? 'selected="selected"' : '' }}>{{ $pelelang->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="user_pemenang_umkm_id">Pemenang:</label>
                            <select name="user_pemenang_umkm_id">
                                @if(is_null($datas->user_pemenang_umkm_id))
                                    @foreach($datas->userinvestorlelangowner->where('usertype_id','=','2')->get() as $pemenang)
                                        <option class="form-control" id="pemenang" value="{{ $pemenang->id }}">{{ $pemenang->name }}</option>
                                    @endforeach
                                @else()
                                    @foreach($datas->userumkmpemenanglelang->where('usertype_id','=','2')->get() as $pemenang)
                                        <option class="form-control" id="pemenang" value="{{ $pemenang->id }}" {{ $datas->user_pemenang_umkm_id == $pemenang->id ? 'selected="selected"' : '' }}>{{ $pemenang->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="lelangstatus_id">Status:</label>
                            <select name="lelangstatus_id">
                                @foreach($datas->lelangstatus->get() as $lelangstatus)
                                    <option class="form-control" id="lelangstatus" value="{{ $lelangstatus->id }}" {{ $datas->lelangstatus_id == $lelangstatus->id ? 'selected="selected"' : '' }}>{{ $lelangstatus->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger" onclick="history.back();">Cancel</button>
                        </div>
                    </form>
                @endforeach
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">

        $('.timepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'

        });

    </script>
@endsection
