@extends('admin.admin')
@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">

        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Edit UMKM kategory</h1>
                <hr>
                @foreach($data as $datas)
                    <form action="{{ route('umkm.update', $datas->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $datas->name }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi:</label>
                            <input type="text" class="form-control" id="description" name="description" value="{{ $datas->description }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger" onclick="history.back();">Cancel</button>
                        </div>
                    </form>
                @endforeach
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection