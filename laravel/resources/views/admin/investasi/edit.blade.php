@extends('admin.admin')
@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">


        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Edit Coaching</h1>
                <hr>
                @foreach($data as $datas)
                    <form action="{{ route('investasi.update', $datas->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $datas->title }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description:</label>
                            <input type="text" class="form-control" id="description" name="description" value="{{ $datas->description }}">
                        </div>
                        <div class="form-group">
                            <label for="nominal">Nominal:</label>
                            <input type="text" class="form-control" id="nominal" name="nominal" value="{{ $datas->nominal }}">
                        </div>
                        <div class="form-group">
                            <label for="inkubator">Verifikator:</label>
                            <select name="inkubator">
                                @foreach($datas->inkubator->where('usertype_id','=','1')->get() as $inkubator)
                                    <option class="form-control" id="inkubator" value="{{ $inkubator->id }}" {{ $datas->inkubator_user_verifikator_id == $inkubator->id ? 'selected="selected"' : '' }}>{{ $inkubator->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Status:</label>
                            <select name="status">
                                @foreach($datas->investasistatus->get() as $status)
                                    <option class="form-control" id="status" value="{{ $status->id }}" {{ $datas->investasistatus_id == $status->id ? 'selected="selected"' : '' }}>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger" onclick="history.back();">Cancel</button>
                        </div>
                    </form>
                @endforeach
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection