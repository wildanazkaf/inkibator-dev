@extends('admin.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Investasi Management</h3>
                            <a href="" class=" btn btn-sm btn-primary">Create</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Nominal</th>
                                    <th>Investor</th>
                                    <th>UMKM</th>
                                    <th>Inkubator Verifikator</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($investasi as $i => $inv)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $inv->title }}</td>
                                        <td>{{ $inv->description }}</td>
                                        <td>{{ $inv->nominal }}</td>
                                        <td>{{ $inv->user->name }}</td>
                                        <td>{{ $inv->umkm->name }}</td>
                                        @if(is_null($inv->inkubator_user_verifikator_id))
                                            <td>Belum terverifikasi</td>
                                        @else()
                                            <td>{{ $inv->inkubator->name }}</td>
                                        @endif
                                        <td>{{ $inv->investasistatus->name }}</td>

                                        <td>
                                            <form action="{{ route('investasi.destroy', $inv->id) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('investasi.edit',$inv->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
