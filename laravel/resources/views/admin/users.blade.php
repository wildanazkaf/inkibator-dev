@extends('admin.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">User Management</h3>
                            <a href="{{ route('user.create') }}" class=" btn btn-sm btn-primary">Create</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Role</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>E-Learning</th>
                                    <th>Forum</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $i => $user)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->usertype->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone }}</td>
                                        @if(is_null($user->is_added_to_elearning) or $user->is_added_to_elearning<1)
                                            <td>Belum terdaftar</td>
                                        @else()
                                            <td>Terdaftar</td>
                                        @endif
                                        @if(is_null($user->is_added_to_forum) or $user->is_added_to_forum<1)
                                            <td>Belum terdaftar</td>
                                        @else()
                                            <td>Terdaftar</td>
                                        @endif
                                        <td>
                                            <form action="{{ route('user.destroy', $user->id) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('user.edit',$user->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
