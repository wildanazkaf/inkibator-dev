<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{url('admin/coaching')}}">
                    <i class="fa fa-table"></i> <span>Coaching Management</span>
                </a>
            </li>
            <li>
                <a href="{{url('admin/investasi')}}">
                    <i class="fa fa-table"></i> <span>Investasi Management</span>
                </a>
            </li>
            <li>
                <a href="{{url('admin/lelang')}}">
                    <i class="fa fa-table"></i> <span>Lelang Management</span>
                </a>
            </li>
            <li>
                    <a href="{{url('admin/umkm')}}">
                    <i class="fa fa-table"></i> <span>UMKM Management</span>
                </a>
            </li>
            <li>
                <a href="{{url('admin/users')}}">
                    <i class="fa fa-table"></i> <span>User Management</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>