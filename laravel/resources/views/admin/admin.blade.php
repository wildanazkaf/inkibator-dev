<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    @include('head')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('header')
    <!-- Left side column. contains the logo and sidebar -->
    @include('admin.sidebar-left')

    @yield('content')

    @include('footer')

    <!-- Control Sidebar -->
    @include('admin.sidebar-right')
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@include('setting')
</body>
</html>
