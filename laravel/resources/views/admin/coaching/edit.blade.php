@extends('admin.admin')
@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">

        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Edit Coaching</h1>
                <hr>
                @foreach($data as $datas)
                    <form action="{{ route('coaching.update', $datas->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="masalah">Ringkasan Masalah:</label>
                            <input type="text" class="form-control" id="masalah" name="masalah" value="{{ $datas->ringkasan_masalah }}">
                        </div>
                        <div class="form-group">
                            <label for="status">Status:</label>
                            <select name="status">
                                @foreach($datas->coachingstatus->get() as $status)
                                    <option class="form-control" id="status" value="{{ $status->id }}" {{ $datas->coachingstatus_id == $status->id ? 'selected="selected"' : '' }}>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="user">User:</label>
                            <select name="user">
                                @foreach($datas->user->where('usertype_id','=','2')->get() as $role)
                                    <option class="form-control" id="user" value="{{ $role->id }}" {{ $datas->user_id == $role->id ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger" onclick="history.back();">Cancel</button>
                        </div>
                    </form>
                @endforeach
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection