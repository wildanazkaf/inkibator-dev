@extends('admin.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Coaching Management</h3>
                            <a href="" class=" btn btn-sm btn-primary">New Coaching</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Ringkasan Masalah</th>
                                    <th>Status</th>
                                    <th>Tipe</th>
                                    <th>User</th>
                                    <th>Coach</th>
                                    <th>Action</th>
                                    <th>Assign</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($coaching as $i => $coach)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $coach->ringkasan_masalah }}</td>
                                        <td>{{ $coach->coachingstatus->name }}</td>
                                        @foreach ($coach->coachingreports as $report)
                                            <td>{{ $report->coachingreporttype->name }}</td>
                                        @endforeach
                                        <td>{{ $coach->user->name }}</td>
                                        @if(is_null($coach->user_coach_id))
                                            <td>Belum di assign</td>
                                        @else()
                                            <td>{{ $coach->userCoach->name }}</td>
                                        @endif
                                        <td>
                                            <form action="{{ route('coaching.destroy',$coach->id) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('coaching.edit',$coach->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                            </form>
                                        </td>
                                        <td><a href="{{ route('coaching.assign',$coach->id) }}" class=" btn btn-sm btn-success">Assign Coach</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
