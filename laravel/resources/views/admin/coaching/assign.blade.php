@extends('admin.admin')
@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">
        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Edit Coaching</h1>
                <hr>


                    <form action="{{ route('coaching.assigned', $coacher[0]->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <select name="coach" id="coach" class="select2-dropdown">
                            @foreach($data as $datas)
                            <option value="{{$datas->id}}">{{$datas->name}}</option>

                            @endforeach
                        </select>
                        <div><br /></div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Assign</button>
                            <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                        </div>
                    </form>
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection