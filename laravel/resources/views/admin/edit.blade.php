@extends('admin.admin')
@section('content')
    <!-- Main Section -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <section class="content">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Anak IT -  Edit Kontak</h1>
                <hr>
                @foreach($data as $datas)
                    <form action="{{ route('user.update', $datas->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="name">Nama:</label>
                            <input type="text" class="form-control" id="usr" name="name" value="{{ $datas->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $datas->email }}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="text" class="form-control" id="password" name="password" onfocus="this.value=''" value="********">
                        </div>
                        <div class="form-group">
                            <label for="phone">Telepon:</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $datas->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat:</label>
                            <input type="textarea" class="form-control" id="address" name="address" value="{{ $datas->address }}">
                        </div>
                        <div class="form-group">
                            <label for="about">Keterangan:</label>
                            <input type="text" class="form-control" id="about" name="about" value="{{ $datas->about }}">
                        </div>
                        <div class="form-group">
                            <label for="is_added_to_elearning">Apakah sudah masuk E-learning:</label><br />
                            <input type="radio" id="is_added_to_elearning" name="is_added_to_elearning" {{ ($datas->is_added_to_elearning==1) ? "checked" : "true" }} value="1">Sudah
                            <input type="radio" id="is_added_to_elearning" name="is_added_to_elearning" {{ ($datas->is_added_to_elearning==0) ? "checked" : "true" }} value="0">Belum
                        </div>
                        <div class="form-group">
                            <label for="is_added_to_forum">Apakah sudah masuk Forum:</label><br />
                            <input type="radio" id="is_added_to_forum" name="is_added_to_forum" {{ ($datas->is_added_to_forum==1) ? "checked" : "true" }} value="1">Sudah
                            <input type="radio" id="is_added_to_forum" name="is_added_to_forum" {{ ($datas->is_added_to_forum==0) ? "checked" : "true" }} value="0">Belum
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            <button type="reset" class="btn btn-md btn-danger" onclick="history.back();">Cancel</button>
                        </div>
                    </form>
                @endforeach
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
@endsection