@extends('coach.coach')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body" style="height: 300px; overflow: auto">
                <div class="header">
                    <strong>Saya</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            2 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Hallo, Saya UMKM
                </div>
                <div class="header">
                    <strong>Coach</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            5 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Hallo, UMKM, Ada yang bisa dibantu?
                </div>
                <div class="header">
                    <strong>Saya</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            4 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Saya ada masalah
                </div>
                <div class="header">
                    <strong>Coach</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            4 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Ada apa?
                </div>
                <div class="header">
                    <strong>Saya</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            4 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Bagaimana cara membuat kemasan yang bagus?
                </div>
                <div class="header">
                    <strong>Coach</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            4 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Begini..
                </div>
                <div class="header">
                    <strong>Saya</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            2 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Terima Kasih
                </div>
                <div class="header">
                    <strong>Coach</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time">
                            1 min a go
                        </span>
                    </small>
                </div>
                <div class="chatcolor">
                    Sama-sama
                </div>
            </div>
            <div class="panel-footer">
                <div class="input-group">
                    <input type="" class="form-control" placeholder="Ketik disisni..." name="">
                    <span class="input-group-btn">

                        <button class="btn btn-warning">Kirim</button>
                    </span>
                </div>
            </div>

        </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection
<style type="text/css">
    .chatcolor{
        background-color: #f0ad4e ;
        padding: 10px;
        border-bottom-left-radius: 24px;
        border-bottom-right-radius: 28px;
        border-top-right-radius: 28px;
        margin-top: 6px;

    }
</style>