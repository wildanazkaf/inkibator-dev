@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


{{--@extends('back.layouts.masterlogin')--}}

{{--@section('content')--}}

    {{--<body class="hold-transition login-page">--}}
    {{--<div class="login-box">--}}
        {{--<div class="login-logo">--}}
            {{--<a href="../../index2.html"><b>Admin</b>LTE</a>--}}
        {{--</div>--}}
        {{--<!-- /.login-logo -->--}}
        {{--<div class="login-box-body">--}}
            {{--<p class="login-box-msg">Sign in to start your session</p>--}}

            {{--<form action="../../index2.html" method="post">--}}
                {{--<div class="form-group has-feedback">--}}
                    {{--<input type="email" class="form-control" placeholder="Email">--}}
                    {{--<span class="glyphicon glyphicon-envelope form-control-feedback"></span>--}}
                {{--</div>--}}
                {{--<div class="form-group has-feedback">--}}
                    {{--<input type="password" class="form-control" placeholder="Password">--}}
                    {{--<span class="glyphicon glyphicon-lock form-control-feedback"></span>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-8">--}}
                        {{--<div class="checkbox icheck">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox"> Remember Me--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.col -->--}}
                    {{--<div class="col-xs-4">--}}
                        {{--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--}}
                    {{--</div>--}}
                    {{--<!-- /.col -->--}}
                {{--</div>--}}
            {{--</form>--}}

            {{--<div class="social-auth-links text-center">--}}
                {{--<p>- OR -</p>--}}
                {{--<a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using--}}
                    {{--Facebook</a>--}}
                {{--<a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using--}}
                    {{--Google+</a>--}}
            {{--</div>--}}
            {{--<!-- /.social-auth-links -->--}}

            {{--<a href="#">I forgot my password</a><br>--}}
            {{--<a href="register.html" class="text-center">Register a new membership</a>--}}

        {{--</div>--}}
        {{--<!-- /.login-box-body -->--}}
    {{--</div>--}}
    {{--<!-- /.login-box -->--}}

{{--@endsection--}}